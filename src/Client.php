<?php
namespace Uniqid;

class Client {
    public function __construct($server) {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_URL, "http://$server/");
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
    }

    public function __destruct() {
        curl_close($this->curl);
    }

    public function newID() {
        $result = curl_exec($this->curl);
        if ($result === FALSE)
            throw new \Exception();
        return (int)$result;
    }

    private $curl;
};
